# About this Code

The code in this repository is part of a tutorial by **0612 TV** entitled **Easy Memo Tables in Python!**, hosted on YouTube.

[![Click to Watch](https://img.youtube.com/vi/0jxhK1dIV_U/0.jpg)](https://www.youtube.com/watch?v=0jxhK1dIV_U "Click to Watch")

# Files

You will find two mutually exclusive files in this repository:

## `Memoizer (Basic).py`

This file mirrors the code written for the tutorial within the video linked above.

While simpler, this rendition has some limitations, most notably the inability to easily use the memo table and the counter for multiple functions as a shared global variable is used. Also, global variables are icky, I guess!

## `Memoizer (Class).py`

A more complex version of the memoizer code that uses Object Oriented Programming techniques to encapsulate memo tables and counters in objects so that you can memoize more than one function at once. The code is annotated with comments. You can also find an example use case within the same file.

# License

This project is licensed under the **Apache License 2.0**. For more details, please refer to LICENSE.txt within this repository.